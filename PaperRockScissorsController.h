#ifndef PAPERROCKSCISSORSCONTROLLER_H
#define PAPERROCKSCISSORSCONTROLLER_H

#include <string>

#include "PaperRockScissors.h"

namespace prs
{

class PaperRockScissorsController final
{
public:
    PaperRockScissorsController();
    ~PaperRockScissorsController() = default;

    void run(unsigned int rounds);

private:
    static std::string figureToString(const PaperRockScissors::Figure& figure);
    static PaperRockScissors::Figure stringToFigure(const std::string& str);

    PaperRockScissors::Figure getUserChoice() const;
    void logChoice(const PaperRockScissors::Figure& player1Choice,
        const PaperRockScissors::Figure& player2Choice) const;
    void logYouWin() const;
    void logYouLose() const;
    void logTie() const;
    void printSummary() const;
    void clear() const;

    int player1Points_;
    int player2Points_;
};

}

#endif // PAPERROCKSCISSORSCONTROLLER_H
