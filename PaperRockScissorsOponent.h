#ifndef PAPERROCKSCISSORSOPONENT_H
#define PAPERROCKSCISSORSOPONENT_H

#include "PaperRockScissors.h"

namespace prs
{

class PaperRockScissorsOponent final
{
public:
    static PaperRockScissors::Figure getChoice();
};

}  // namespace prs

#endif // PAPERROCKSCISSORSOPONENT_H
