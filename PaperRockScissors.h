#ifndef PAPERROCKSCISSORS_H
#define PAPERROCKSCISSORS_H

namespace prs
{

class PaperRockScissors final
{
public:
    enum class Winner
    {
        Player1,
        Player2,
        Tie
    };

    enum class Figure
    {
        Paper    = 0,
        Rock     = 1,
        Scissors = 2,
        Invalid
    };

    static Winner getWinner(const Figure& player1Choice,
        const Figure& player2Choice);
};

}  // namespace prs



#endif // PAPERROCKSCISSORS_H
