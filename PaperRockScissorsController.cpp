#include "PaperRockScissorsController.h"

#include <iostream>
#include <map>
#include <time.h>

#include "PaperRockScissorsOponent.h"

namespace prs
{

PaperRockScissorsController::PaperRockScissorsController()
{
    srand(time(NULL));
}

void PaperRockScissorsController::run(unsigned int rounds)
{
    unsigned int roundsLeft = rounds;
    clear();
    std::cout << "Welcome to Paper Rock Scissors game!" << std::endl;
    std::cin.ignore();
    while (roundsLeft > 0)
    {
        clear();
        std::cout << "Round: " << (rounds - roundsLeft + 1)
            << " / " << rounds << std::endl;
        std::cout << "Press p for Paper, r for Rock or s for Scissors" << std::endl;
        auto player1Choice = getUserChoice();
        auto player2Choice = PaperRockScissorsOponent::getChoice();
        auto winner = PaperRockScissors::getWinner(player1Choice, player2Choice);
        logChoice(player1Choice, player2Choice);

        switch (winner)
        {
            case PaperRockScissors::Winner::Player1:
            {
                logYouWin();
                std::cin.ignore();
                player1Points_++;
                break;
            }

            case PaperRockScissors::Winner::Player2:
            {
                logYouLose();
                std::cin.ignore();
                player2Points_++;
                break;
            }

            case PaperRockScissors::Winner::Tie:
            {
                logTie();
                std::cin.ignore();
                break;
            }
        }
        std::cin.ignore();
        roundsLeft--;
    }
    clear();
    printSummary();
}

std::string PaperRockScissorsController::figureToString(const PaperRockScissors::Figure& figure)
{
    switch (figure)
    {
        case PaperRockScissors::Figure::Paper: return "Paper";
        case PaperRockScissors::Figure::Rock: return "Rock";
        case PaperRockScissors::Figure::Scissors: return "Scissors";
    }
    return "UknownFigure";
}

PaperRockScissors::Figure PaperRockScissorsController::stringToFigure(const std::string& str)
{
    std::map<std::string, PaperRockScissors::Figure> table =
    {{"p", PaperRockScissors::Figure::Paper},
     {"r", PaperRockScissors::Figure::Rock},
     {"s", PaperRockScissors::Figure::Scissors}};

    if (table.count(str) == 0)
    {
        return PaperRockScissors::Figure::Invalid;
    }

    return table.at(str);
}

PaperRockScissors::Figure PaperRockScissorsController::getUserChoice() const
{
    std::string strChoice;
    while (stringToFigure(strChoice) == PaperRockScissors::Figure::Invalid)
    {
        std::cin >> strChoice;
    }

    return stringToFigure(strChoice);
}

void PaperRockScissorsController::logChoice(const PaperRockScissors::Figure& player1Choice,
    const PaperRockScissors::Figure& player2Choice) const
{
    std::cout << "Your choice: " << figureToString(player1Choice) << std::endl
        << "Oponent choice: " << figureToString(player2Choice) << std::endl;
}

void PaperRockScissorsController::logYouWin() const
{
    std::cout << "Congrats, you win!" << std::endl;
}

void PaperRockScissorsController::logYouLose() const
{
    std::cout << "You lose!" << std::endl;
}

void PaperRockScissorsController::logTie() const
{
    std::cout << "Nobody wins this time!" << std::endl;
}

void PaperRockScissorsController::printSummary() const
{
    std::cout << "You: " << player1Points_
        << " Oponent: " << player2Points_
        << std::endl;
}

void PaperRockScissorsController::clear() const
{
    std::cout << "\x1B[2J\x1B[H";
}

}  // namespace prs
