#include <iostream>

#include <boost/program_options.hpp>

#include "PaperRockScissorsController.h"

namespace po = boost::program_options;

int main(int argc, char** argv)
{
    try
    {
        po::options_description desc{"Options"};
        desc.add_options()
            ("help", "produce help message")
            ("times", po::value<int>()->default_value(5), "number of game rounds");

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);

        if (vm.count("help"))
        {
            std::cout << desc << "\n";
            return 1;
        }

        if (vm.count("times"))
        {
            const int rounds = vm["times"].as<int>();
            std::cout << "Execute times : " << rounds << '\n';
            prs::PaperRockScissorsController prsLoop;
            prsLoop.run(rounds);
        }
    }
    catch (const po::error &ex)
    {
        std::cerr << ex.what() << '\n';
    }
    return 0;
}

