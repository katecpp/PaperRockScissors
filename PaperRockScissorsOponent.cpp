#include "PaperRockScissorsOponent.h"

#include <cstdlib>

namespace prs
{

PaperRockScissors::Figure PaperRockScissorsOponent::getChoice()
{
    unsigned int choiceIdx = rand() % 3;
    return static_cast<PaperRockScissors::Figure>(choiceIdx);
}



}  // namespace prs
