#include "PaperRockScissors.h"

#include <array>
#include <assert.h>

namespace prs
{

PaperRockScissors::Winner PaperRockScissors::getWinner(
    const PaperRockScissors::Figure& player1Choice,
    const PaperRockScissors::Figure& player2Choice)
{
    static std::array<std::array<Winner, 3>, 3> resultTable =
//                      Player1:
//    Rock              Paper               Scissors
    {{{Winner::Tie,     Winner::Player1,    Winner::Player2},   // Player2: Rock
     {Winner::Player2,  Winner::Tie,        Winner::Player1},   // Player2: Paper
     {Winner::Player1,  Winner::Player2,    Winner::Tie}}};     // Player2: Scissors

    const int player1Idx = static_cast<int>(player1Choice);
    const int player2Idx = static_cast<int>(player2Choice);
    assert(player1Idx >= 0 && player1Idx <= 2);
    assert(player2Idx >= 0 && player2Idx <= 2);

    return resultTable[player1Idx][player2Idx];
}

}  // namespace prs
